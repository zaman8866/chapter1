/*//01.Hello world print in C.
#include<stdio.h>
#include<math.h>
int main(){
    printf("1.Hello world \n");
    printf("2.Hello world \n");
    printf("3.Hello world \n");
    printf("4.Hello world \n");
    printf("5.Hello world \n");
    printf("6.Hello world \n");
    printf("7.Hello world \n");
    printf("8.Hello world \n");
    printf("9.Hello world \n");
    printf("10.Hello world \n");

    return 0;
}
*/
/*//02.Variable print in C.
#include<stdio.h>
#include<math.h>
int main(){
    int i = 20;//variable ar value change kora jay only constant variable not change.
    float pi = 3.14;//variable case sensetive.
    char ch = '*';//variable should be meaningful.
    printf("%d\n", i);
    printf("%f\n", pi);
    printf("%c\n", ch);
    return 0;
}
*/
/*//3.C contain many data type but not contain string and boolean. 
#include<stdio.h>
#include<math.h>
int main(){
    int i = 10; // int datatype can store positve/negative/0.
    float pi = 3.14; // float datatype can store decimal value.
    char stat = '*'; // char datatype can store special character and single alphabet.
    printf("%c", stat);

    return 0;
}
*/


/*
//4.Constan:Theree constan:1.int constant 2.float const 3.char constant
#include<stdio.h>
#include<math.h>
int main(){
    const int i = -10; //valu not chagble
    const float pi = 3.14; //decimal value but not changable 
    const char ch = 'a'; //not changable small letter capital letter are not same
    return 0;
}
*/
/*
//5.Keyword
#include<stdio.h>
#include<math.h>
int main(){
    int i = 0; //C programming contain 32 keyword
    float pi = 3.14; //dont use keyword name of as variable
    char ch = 'n'    // dont use keyword as name of any funciotn name,

        return 0;
}
*/
/*//6.Program Structure
#include<stdio.h>//preprosessor Directives
#include<math.h>//math function use korar jonno ay file lage
int main(){//Execution stat with main (){} function se. 

    return 0;// return 0 means 0 errors main function int ar sathe aytar relation 
}
*/
/*
//7.Comments how to declear comments
#include<stdio.h>
#include<math.h>
int main(){
    //comments is tow type 1. single sine comments 2. multiline comments
    return 0;
}
*/
/*//8a.Output

#include<stdio.h>
#include<math.h>
int main(){
    printf("Hello world \n");// \n means next line
    printf("Hello world \n");
    printf("Hello world \n");

    return 0;
}
*/
/*
//8b.Output
#include<stdio.h>
#include<math.h>
int main(){
    int i = 10;// output kn format a dekhabe %d/%f/%c
    float pi = 20.20;// j data type value nibo oy type e value output a dekhbo
    char ch = '*';
    printf("%c", ch);//printf() hosche akta libray function
    return 0; 
}
*/
/*//9.input
#include<stdio.h>
#include<math.h>
int main(){
    int i;
    printf("Enter any number: ");
    scanf("%d", &i);
    printf("age is: %d ", i);

    return 0;
}
*/
/*//10a. Sum programm
#include<stdio.h>
#include<math.h>
int main(){
    int a, b;
    printf("Enter first number: ");
    scanf("%d", &a);

    printf("Enter second number: ");
    scanf("%d", &b);

    int sum = a + b;
    printf("%d", sum);

    return 0;
}
*/
/*//10b.another sum/divide/multipication
#include<stdio.h>
#include<math.h>
int main(){
    int a, b;
    printf("Enter first number: ");
    scanf("%d", &a);

    printf("Enter second number: ");
    scanf("%d", &b);

    printf("sum value is: %d", a+b );//sum no need extra variable declear.


    return 0;

    return 0;
}
*/
/*
//11.Compilation
#include<stdio.h>//c programm first compile the file...jodi kno error na thake then ./a.exe file hoy ja computer bujhe
#include<math.h>
int main(){

    return 0;
}
*/
/*//Practice proble1:
//Write a programm to calculate of a square(side is given)
#include<stdio.h>
#include<math.h>
int main(){
    int side;
    printf("Enter side: ");
    scanf("%d", &side);

    printf("sum value is: %d", side *side );//sum no need extra variable declear.


    return 0;
}
*/

/*//Practice proble2:
//Write a programm to calculate area of a circule(radious is given)
#include<stdio.h>
#include<math.h>
int main(){
    float radious;
    printf("Enter radious: ");
    scanf("%f", & radious);

    printf("sum value is: %f", 3.14 * radious * radious );//sum no need extra variable declear.


    return 0;
}
*/
/*//HomeWork
//1. Write a program to calculate perimeter of rectangle(Take a sides, a & b, form user).
//2. Take a number(n) form user & output its cube(n*n*n).
//3. Write comments for programms a & b.
//4. convert ferenhight to celcious
//5. convert celcious to feranhight.
*/


